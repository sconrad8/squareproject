# README #

### How to run ###

* Download and unzip project
* Open Terminal and navigation into first SquareApp folder within directory of project. (This location should include the Podfile). Run "pod install"
* Open SquareApp.xcworkspace to launch the project in Xcode
* Within Xcode, in the top left, select iPhone X or any iPhone simulator you desire, click run button. App should launch.
* If error occurs because no signing team has been selected, navigate to Targets and select SquareApp. Navigate to "Signing & Capabilities" tab. Select Signing team.
* The app can be run with various endpoints by navigating to the APIService file and updating the fetchEmployees route with either employees, employeesMalformed, or employeesEmpty.

### Build Tools & Versions Used ###

* Xcode 11.5
* iOS 13.5
* Swift 5

### Focus Areas ###

Most of my focus went toward the architecture of the project. One of my goals for this project was to set up an architecture that separates different layers of the code and is testable. I was able to achieve this using MVVM. This structure is very useful for separating the business logic from the user interface. Data models are contained in the Model layer. The UI components are contained in the View layer. The business logic is contained in the ViewModel which can easily be tested.

Aside from the Model, View, and ViewModel layers, I have also added a Utility and Networking layer. The Utility layer is used for any extensions or helper classes/protocols. The Networking layer is used to handle any API requests. I have created a struct called HttpRequest with a single static method that can be used to perform requests. At the moment, this method can only handle get requests, but it can easily be updated to allow any http request. A singleton class called APIService has also been created which includes methods that perform the api requests, add any info to the request, and handle the response. APIService is a subclass of APIServiceProtocol. APIServiceProtocol has been created, so the APIService can be mocked while testing.

I also focused on a realistic user experience. I didn't aim for very detailed TableViewCells because that may defeat the purpose of an employee directory. I chose to show critical, yet simple information for each TableViewCell. The small photo, full name, and team is the only information displayed for each cell. This keeps the list simple, but gives just enough information for the user to understand who each employee is. The small photo was used because the employee images being displayed are small, and this would allow for quicker downloading of images compared to the large images. I can see this being enhanced by tapping a cell to navigate to an Employee Details view which would show more info about the Employee.

Architecture and user experience are my strengths; however, I am always looking to improve my knowledge in these areas and others. Luckily, Square seems like a great place to do just that. 

### Copied-in code or copied-in dependencies ###

* UIViewExt -> anchor: This method was copied from one of my personal projects.
* ReusableView: This method was copied from one of my personal projects.
* RxSwift/RxCocoa/RxTest: Added to improve reactivity. Works great with MVVM. Easy to test.
* Kingfisher: Added to handle downloading and caching images. Kingfisher is lightweight and can be used to handle image loading and failure. I considered building this myself using NSCache as a memory cache and FileManager as a disk cache; however, kingfisher does this as well, so it was a better use of my time to use Kingfisher.

### Tablet / phone focus ###

* iPhone was focused. Should work with any iPhone on Dark/Light mode.

### How long I spent on the project ###

I spent around 4.5 hours.

### Anything else? ###

I chose to develop the UI with code instead of storyboards or NIBs because it allows me to have better awareness and control over the UI. It is easier to know exactly which component properties have been set, and how each component is being used. I have also found that it is easy to reuse views and add polymorphism. There may also be a performance gain because Storyboards and NIBs have to be translated into code. Merge conflicts are also much easier to manage. However, I have noticed some issues with this strategy. I have found that prototyping and perfecting the design can take a bit longer due to increased trial and error as UI changes are made, but as I've gained more experience, I have found this to be less of an issue. Refactoring/debugging can also be a challenge with more complex UI code. I do feel like the strengths outweigh the weaknesses, though.

I have a method within EmployeeTableViewCell: setup(with employee: Employee). I was battling between passing the Employee Model to this view or creating and passing a CellViewModel which includes properties limited to what is displayed in the UI. Technically, with MVVM, Model should remain separate from the View, but with this project I decided to just pass the Employee model for simplicity. Also, by passing the Employee, there is no need to map the list of Employees to a list of CellViewModels. Although insignificant, there is a minor performance gain by not choosing to map the list.

I really appreciate the opportunity to interview with Square, and I look forward to what the future holds.
