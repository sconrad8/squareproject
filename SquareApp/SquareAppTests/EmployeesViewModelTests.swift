//
//  EmployeesViewModelTests.swift
//  SquareAppTests
//
//  Created by Samuel Conrad on 7/21/20.
//  Copyright © 2020 sconrad. All rights reserved.
//

import XCTest
@testable import SquareApp
import RxSwift
import RxTest

class EmployeesViewModelTests: XCTestCase {

    var viewModel: EmployeesViewModel!
    var apiService: MockAPIService!
    var scheduler: TestScheduler!
    var disposeBag: DisposeBag!
    
    override func setUp() {
        super.setUp()
        
        apiService = MockAPIService()
        viewModel = EmployeesViewModel(apiService: apiService)
        scheduler = TestScheduler(initialClock: 0)
        disposeBag = DisposeBag()
    }
    
    override func tearDown() {
        super.tearDown()
        
        viewModel = nil
        apiService = nil
        scheduler = nil
        disposeBag = nil
    }
    
    func testLoading() {
        let observer = scheduler.createObserver(EmployeesTableState.self)
        
        viewModel.tableStateDriver
            .drive(observer)
            .disposed(by: disposeBag)
        
        XCTAssertEqual(observer.events.first?.value.element, EmployeesTableState.loading)
    }
    
    func testFetchWithEmployees() {
        let observer = scheduler.createObserver(EmployeesTableState.self)
        
        let employees = [Employee(uuid: "123", fullName: "John Cena", phoneNumber: nil, emailAddress: "John_Cena@square.com", biography: nil, photoUrlSmall: nil, photoUrlLarge: nil, team: "WWE", type: .contractor)]
        apiService.employeesResponseToReturn = EmployeesResponse(employees: employees)
        viewModel.fetchData()
        
        viewModel.tableStateDriver
            .drive(observer)
            .disposed(by: disposeBag)
        
        XCTAssertEqual(observer.events.last?.value.element, EmployeesTableState.items(employees: employees))
    }
    
    func testFetchWithNoEmployees() {
        let observer = scheduler.createObserver(EmployeesTableState.self)
        
        apiService.employeesResponseToReturn = EmployeesResponse(employees: [])
        viewModel.fetchData()
        
        viewModel.tableStateDriver
            .drive(observer)
            .disposed(by: disposeBag)
        
        XCTAssertEqual(observer.events.last?.value.element, EmployeesTableState.items(employees: []))
    }
    
    func testFetchWithError() {
        let observer = scheduler.createObserver(EmployeesTableState.self)
        
        viewModel.fetchData()
        
        viewModel.tableStateDriver
            .drive(observer)
            .disposed(by: disposeBag)
        
        XCTAssertEqual(observer.events.last?.value.element, EmployeesTableState.error)
    }

}

