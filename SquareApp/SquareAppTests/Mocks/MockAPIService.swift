//
//  MockAPIService.swift
//  SquareAppTests
//
//  Created by Samuel Conrad on 7/21/20.
//  Copyright © 2020 sconrad. All rights reserved.
//

class MockAPIService: APIServiceProtocol {
    
    var employeesResponseToReturn: EmployeesResponse?
    
    func fetchEmployees(completion: @escaping (EmployeesResponse) -> (), incompletion: @escaping () -> ()) {
        if let employeesResponse = employeesResponseToReturn {
            completion(employeesResponse)
        } else {
            incompletion()
        }
    }
    
}
