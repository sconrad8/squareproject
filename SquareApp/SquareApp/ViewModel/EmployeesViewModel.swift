//
//  EmployeesViewModel.swift
//  SquareApp
//
//  Created by Samuel Conrad on 7/21/20.
//  Copyright © 2020 sconrad. All rights reserved.
//

import RxCocoa

final class EmployeesViewModel {
    
    private let apiService: APIServiceProtocol
    
    // MARK: - Relays
    private let tableStateRelay = BehaviorRelay<EmployeesTableState>(value: .loading)
    
    // MARK: - Drivers
    var tableStateDriver: Driver<EmployeesTableState> {
        return tableStateRelay.asDriver()
    }
    
    // MARK: -
    
    init(apiService: APIServiceProtocol) {
        self.apiService = apiService
    }
    
    func fetchData() {
        apiService.fetchEmployees(completion: { [weak self] response in
            self?.tableStateRelay.accept(.items(employees: response.employees.sorted { $0.fullName.lowercased() < $1.fullName.lowercased() }))
        }) { [weak self] in
            self?.tableStateRelay.accept(.error)
        }
    }
    
}

//MARK: -

enum EmployeesTableState {
    case items(employees: [Employee])
    case loading
    case error
}

extension EmployeesTableState: Equatable {
    static func == (lhs: EmployeesTableState, rhs: EmployeesTableState) -> Bool {
        switch (lhs, rhs) {
        case (.items(let lhsEmployees), .items(let rhsEmployees)):
            return lhsEmployees == rhsEmployees
        case (.loading, .loading):
            return true
        case (.error, .error):
            return true
        default:
            return false
        }
    }
}
