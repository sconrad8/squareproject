//
//  SceneDelegate.swift
//  SquareApp
//
//  Created by Samuel Conrad on 7/20/20.
//  Copyright © 2020 sconrad. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let scene = (scene as? UIWindowScene) else { return }
        
        let window = UIWindow(windowScene: scene)
        
        let rootViewController = UINavigationController(rootViewController: EmployeesTableViewController(viewModel: EmployeesViewModel(apiService: APIService.shared)))
        window.rootViewController = rootViewController
        
        self.window = window
        window.makeKeyAndVisible()
    }

}

