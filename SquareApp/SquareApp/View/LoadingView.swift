//
//  LoadingView.swift
//  SquareApp
//
//  Created by Samuel Conrad on 7/21/20.
//  Copyright © 2020 sconrad. All rights reserved.
//

import UIKit

class LoadingView: UIView {
    
    init() {
        super.init(frame: .zero)
        
        let activityIndicator = UIActivityIndicatorView(style: .large)
        activityIndicator.startAnimating()
        
        addSubview(activityIndicator)
        activityIndicator.anchor(top: safeAreaLayoutGuide.topAnchor, left: leftAnchor, bottom: safeAreaLayoutGuide.bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
