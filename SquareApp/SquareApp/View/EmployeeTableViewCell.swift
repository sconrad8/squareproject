//
//  EmployeeTableViewCell.swift
//  SquareApp
//
//  Created by Samuel Conrad on 7/21/20.
//  Copyright © 2020 sconrad. All rights reserved.
//

import UIKit

class EmployeeTableViewCell: UITableViewCell, ReusableView {
    
    private let photoImageView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        iv.layer.cornerRadius = 13
        iv.layer.masksToBounds = true
        return iv
    }()

    private let nameLabel = UILabel()

    private let teamLabel: UILabel = {
        let label = UILabel()
        label.textColor = .secondaryLabel
        return label
    }()
    
    private lazy var stackView: UIStackView = {
        let sv = UIStackView(arrangedSubviews: [nameLabel, teamLabel])
        sv.axis = .vertical
        return sv
    }()
    
    private let bottomSeparator: UIView = {
        let view = UIView()
        view.backgroundColor = .separator
        return view
    }()
    
    //MARK: -
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Setup
    
    private func setupView() {
        contentView.addSubview(photoImageView)
        photoImageView.anchor(top: nil, left: contentView.leftAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 20, paddingBottom: 10, paddingRight: 0, width: 50, height: 50)
        photoImageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true

        contentView.addSubview(stackView)
        stackView.anchor(top: nil, left: photoImageView.rightAnchor, bottom: nil, right: contentView.rightAnchor, paddingTop: 0, paddingLeft: 10, paddingBottom: 0, paddingRight: 10, width: 0, height: 0)
        stackView.centerYAnchor.constraint(equalTo: photoImageView.centerYAnchor).isActive = true
        
        contentView.addSubview(bottomSeparator)
        bottomSeparator.anchor(top: nil, left: stackView.leftAnchor, bottom: contentView.bottomAnchor, right: contentView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 1)
    }
    
    func setup(with employee: Employee) {
        if let urlString = employee.photoUrlSmall {
            photoImageView.setImage(with: urlString, failureImage: UIImage(imageLiteralResourceName: "profilePlaceholder"))
        }
        nameLabel.text = employee.fullName
        teamLabel.text = employee.team
    }
    
}
