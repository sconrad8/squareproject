//
//  EmployeesTableViewController.swift
//  SquareApp
//
//  Created by Samuel Conrad on 7/20/20.
//  Copyright © 2020 sconrad. All rights reserved.
//

import UIKit
import RxSwift

class EmployeesTableViewController: UITableViewController {
    
    private let viewModel: EmployeesViewModel
    private let disposeBag = DisposeBag()
    
    private var items = [Employee]()
    
    init(viewModel: EmployeesViewModel) {
        self.viewModel = viewModel
        
        super.init(style: .plain)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationBar()
        setupTableView()
        bindViewModel()
        
        viewModel.fetchData()
    }
    
    private func bindViewModel() {
        viewModel.tableStateDriver
            .drive(onNext: { [unowned self] state in
                self.items.removeAll()
                
                switch state {
                case let .items(items):
                    self.items = items
                    
                    if items.count == 0 {
                        self.tableView.backgroundView = EmployeesEmptyView()
                    } else {
                        self.tableView.backgroundView = nil
                    }
                case .loading:
                    self.tableView.backgroundView = LoadingView()
                case .error:
                    self.tableView.backgroundView = ErrorView()
                }
                
                self.tableView.reloadSections(IndexSet(integer: 0), with: .fade)
            })
            .disposed(by: disposeBag)
    }
    
    // MARK: - Navigation Bar
    
    private func setupNavigationBar() {
        navigationItem.title = "Employee Directory"
    }
    
    // MARK: - TableView
    
    private func setupTableView() {
        tableView.register(EmployeeTableViewCell.self, forCellReuseIdentifier: EmployeeTableViewCell.reuseID)
        tableView.allowsSelection = false
        tableView.separatorStyle = .none
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: EmployeeTableViewCell.reuseID, for: indexPath) as? EmployeeTableViewCell else {
            return UITableViewCell()
        }
        
        cell.setup(with: items[indexPath.row])
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
}
