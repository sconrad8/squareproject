//
//  ReusableView.swift
//  SquareApp
//
//  Created by Samuel Conrad on 7/21/20.
//  Copyright © 2020 sconrad. All rights reserved.
//

protocol ReusableView {
    static var reuseID: String { get }
}

extension ReusableView where Self: AnyObject {
    static var reuseID: String {
        return String(describing: self)
    }
}
