//
//  UIImageViewExt.swift
//  SquareApp
//
//  Created by Samuel Conrad on 7/21/20.
//  Copyright © 2020 sconrad. All rights reserved.
//

import UIKit
import Kingfisher

extension UIImageView {
    
    func setImage(with urlString: String, failureImage: UIImage? = nil) {
        guard let url = URL(string: urlString) else {
            image = failureImage
            return
        }
        
        var options = KingfisherOptionsInfo()
        
        if let failureImage = failureImage {
            options.append(.onFailureImage(failureImage))
        }
        
        kf.indicatorType = .activity
        kf.setImage(with: url, options: options)
    }
    
}
