//
//  APIService.swift
//  SquareApp
//
//  Created by Samuel Conrad on 7/20/20.
//  Copyright © 2020 sconrad. All rights reserved.
//

import Foundation

class APIService: APIServiceProtocol {
    
    static let shared = APIService()
    
    func fetchEmployees(completion: @escaping (EmployeesResponse) -> (), incompletion: @escaping () -> ()) {
        HttpRequest.perform(route: .employees, method: .get, completion: { data in
            do {
                completion(try JSONDecoder().decode(EmployeesResponse.self, from: data))
            } catch {
                incompletion()
            }
        }, incompletion: incompletion)
    }
    
}
