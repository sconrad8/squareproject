//
//  HttpRequest.swift
//  SquareApp
//
//  Created by Samuel Conrad on 7/20/20.
//  Copyright © 2020 sconrad. All rights reserved.
//

import Foundation

struct HttpRequest {
    
    static let urlSession = URLSession(configuration: .default)
    static let basePath = "https://s3.amazonaws.com/sq-mobile-interview/"
    
    static func perform(route: HttpRoute, method: HttpMethod, completion: @escaping (Data) -> (), incompletion: @escaping () -> ()) {
        guard let url = URL(string: "\(basePath)\(route.rawValue)") else {
            incompletion()
            return
        }
        
        let request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: 10)
        urlSession.dataTask(with: request, completionHandler: { (data, response, error) in
            if let error = error {
                NSLog("HttpRequest Error: %@", error.localizedDescription)
                incompletion()
                return
            }
            
            guard let response = response as? HTTPURLResponse, response.statusCode == 200, let data = data else {
                incompletion()
                return
            }
            
            completion(data)
        }).resume()
    }
    
}
