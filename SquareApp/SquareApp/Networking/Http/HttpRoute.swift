//
//  HttpRoute.swift
//  SquareApp
//
//  Created by Samuel Conrad on 7/20/20.
//  Copyright © 2020 sconrad. All rights reserved.
//

enum HttpRoute: String {
    case employees = "employees.json"
    case employeesMalformed = "employees_malformed.json"
    case employeesEmpty = "employees_empty.json"
}
