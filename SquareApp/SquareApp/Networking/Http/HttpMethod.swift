//
//  HttpMethod.swift
//  SquareApp
//
//  Created by Samuel Conrad on 7/20/20.
//  Copyright © 2020 sconrad. All rights reserved.
//

enum HttpMethod: String {
    case get = "GET"
}

