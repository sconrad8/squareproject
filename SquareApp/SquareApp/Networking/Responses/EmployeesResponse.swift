//
//  EmployeesResponse.swift
//  SquareApp
//
//  Created by Samuel Conrad on 7/20/20.
//  Copyright © 2020 sconrad. All rights reserved.
//

class EmployeesResponse: Decodable {
    
    let employees: [Employee]
    
    init(employees: [Employee]) {
        self.employees = employees
    }
    
}
