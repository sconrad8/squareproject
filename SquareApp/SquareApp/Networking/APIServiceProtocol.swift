//
//  APIServiceProtocol.swift
//  SquareApp
//
//  Created by Samuel Conrad on 7/20/20.
//  Copyright © 2020 sconrad. All rights reserved.
//

protocol APIServiceProtocol {
    func fetchEmployees(completion: @escaping (EmployeesResponse) -> (), incompletion: @escaping () -> ())
}
